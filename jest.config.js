module.exports = {
    preset: "jest-playwright-preset",
    // testMatch: ["**/__tests__/**/*.+(ts|js)", "**/?(*.)+(spec|test).+(ts|js)"],
    testMatch: ["**/test/automation_practice/**/*.+(ts|js)"],
    transform: {
      "^.+\\.(ts)$": "ts-jest",
    },
    testEnvironmentOptions: {
      "jest-playwright": {
        browsers: ["chromium"],
        launchOptions: {
          headless: false,
          args: ["--start-maximized"]
        },
      },
    }
  };