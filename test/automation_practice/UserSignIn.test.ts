import { chromium, Browser, Page } from "../../../node_modules/playwright";
import { AuthenticationPage } from "../../src/pages/automation_practice/AuthenticationPage";
import { HomePage } from "../../src/pages/automation_practice/HomePage";



describe('User Sign In tests', () => {

    let browser: Browser;
    let page: Page;
    beforeAll(async () => {
      browser = await chromium.launch();
    });
    beforeEach(async () => {
      page = await browser.newPage();
      await page.goto("http://automationpractice.com/index.php");
    })
  
    it("Sign in To Automation Practice", async () => {
      let authenticationPage = new AuthenticationPage(page);
      let homePage = new HomePage(page);
    //   await page.goto("http://automationpractice.com/index.php");
      homePage.clickSignIn();
      await authenticationPage.userSignIn("test-amit@test.com", "123456789");
      console.log("Hello World, first test run");
    })
  
    afterEach(async () => {
      await page.close();
    });
  
    afterAll(async () => {
      await browser.close();
    });
  
  })