import { chromium, Browser, Page } from "../../node_modules/playwright";
import { WThreeSchools } from "../src/pages/WThreeSchools.page";
// import data from "../src/data/config.json";

describe('W3 Schools tests', () => {

  let browser: Browser;
  let page: Page;
  beforeAll(async () => {
    browser = await chromium.launch();
  });
  beforeEach(async () => {
    page = await browser.newPage();
  })

  it("Read table data from W3 Schools User data", async () => {
    let wThreeSchools = new WThreeSchools(page);
    await page.goto("https://www.w3schools.com/html/html_tables.asp");
    const allUsers = await wThreeSchools.readTableData();
    console.log(`Read all the users data : ${allUsers}`); 
    console.log("Hello World, first test run")
  })

  afterEach(async () => {
    await page.close();
  });

  afterAll(async () => {
    await browser.close();
  });

})
