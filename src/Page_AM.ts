import { chromium, Browser, Page } from "playwright";


let browser: Browser;
beforeAll(async () => {
    console.log("BeforeAll running..! ")
    browser = await chromium.launch();
});
afterAll(async () => {
    console.log("AfterAll running..! ")
    await browser.close();
});

let page: Page;
beforeEach(async () => {
    page = await browser.newPage();
});
afterEach(async () => {
    await page.close();
});