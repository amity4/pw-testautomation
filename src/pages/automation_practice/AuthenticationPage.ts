import {Page} from "playwright";
import {logger} from "../../utils/logger/logger_config";


export class AuthenticationPage {

    private page: Page;
    email = "#email";
    passwordElement = "#passwd";
    submitLogIn = "#SubmitLogin";

    constructor(page: Page) {
        this.page = page;
    }

    async userSignIn(userName: string, password: string){
        this.page.fill(this.email, userName);
        this.page.fill(this.passwordElement, password)
        await this.page.click(this.submitLogIn);
    }

}