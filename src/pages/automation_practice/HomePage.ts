import {Page} from "playwright";
import {logger} from "../../utils/logger/logger_config";

export class HomePage {
    constructor(page: Page) {
        this.page = page;
    }
    private page: Page;
    signIn = "class=login";
    womenCategory = "xpath=//*[@class='sf-with-ul' and @title='Women']";
    womenCategoryTShirts = "xpath=//*[contains(@class,'submenu-container')]//a[@title='T-shirts']";


    public async clickSignIn(){
        await this.page.click(this.signIn);
    }
}