import {Page} from "../../../node_modules/playwright";
import {logger} from "../utils/logger/logger_config";

export class WThreeSchools {
    constructor(page: Page) {
        this.page = page;
    }
    private page: Page;
    private tableExample = 'xpath=//*[@id="customers"]/tbody/tr';

    async readTableData(){
        logger.info('reading table data')
        var allusers = [];
        const rows_1 = await this.page.$$(this.tableExample);
        console.log(`Number of Rows : ${rows_1.length}`)
        for (const p of rows_1) {
            const users1 = await this.page.$$eval('xpath=//*[@id="customers"]/tbody/tr', (p) => {
                return p.map(user => {
                    const company = user.querySelector('th:nth-child(1)');
                    const contact = user.querySelector('th:nth-child(2)');
                    const country = user.querySelector('th:nth-child(3)');
                    return {
                        Company: company.textContent !== null ? company.textContent.trim() : 'dummy',
                        Contact: contact.textContent !== null ? contact.textContent.trim() : 'dummy',
                        Country: country.textContent !== null ? country.textContent.trim() : 'dummy'
                    };
                    console.log(`$$$ User details : $$$ :  ${user}`)
                });
            });
            console.log(`$$$ User details : $$$ :  ${users1}`)
            allusers.push(users1);
        }
        return allusers;
    }
}